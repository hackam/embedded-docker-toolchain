# embedded-toolchain-docker

A docker image for building embedded projects with CI pipeline. Mainly built for atlassian bibucket pipeline.

## Included Tools

1. Attolic True Studio v9.3.0 toolchain setup. (Similar to the IDE for linux)
2. PlatformIO Core CLI
3. Platform IO `espressif8266` and `espressif32` packages
4. NodeJs 10.15.2
5. gulp
6. binary_merger.py (custom python tool for merging two binary files with specified offset,useful for creating a combined firmware and spiffs binary for platform io espressif builds)


## How to use

Option 1: Obtain from docker hub `https://cloud.docker.com/repository/docker/amente/embedded-toolchain-docker/tags`

Option 2: Clone this repo and build from source