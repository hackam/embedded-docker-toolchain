import argparse

parser = argparse.ArgumentParser(description='Merge two binary files with padding.')
parser.add_argument('file1')
parser.add_argument('file2')
parser.add_argument('offset', default='0x300000')

args = parser.parse_args()

with open('combined.bin', 'wb') as f,  open(args.file1, 'rb') as f1,  open(args.file2, 'rb') as f2:
    f.write(f1.read())
    for i in range(int(args.offset, 0) - f.tell()):
        f.write(b'\xFF')
    f.write(f2.read())
