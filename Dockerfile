FROM ubuntu:20.04
MAINTAINER Amente Bekele

ARG DEBIAN_FRONTEND=noninteractive

# Install Tools from TrueStudio
RUN mkdir -p /opt/attolic_ts
COPY attolic_ts.tar.gz /opt/attolic_ts/
RUN tar --strip-components=1 -zxf /opt/attolic_ts/attolic_ts.tar.gz -C /opt/attolic_ts
RUN rm /opt/attolic_ts/attolic_ts.tar.gz
ENV PATH=$PATH:/opt/attolic_ts/ARMTools/bin:/opt/attolic_ts/Tools
ENV ATOLLIC_TOOLS_DIR=/opt/attolic_ts/Tools

# Install Generic Tools
RUN apt-get update && apt-get install -y --no-install-recommends wget zip unzip git make \
 srecord bc xz-utils gcc curl ca-certificates python3 python3-distutils

RUN curl -fsSL https://raw.githubusercontent.com/platformio/platformio-core-installer/master/get-platformio.py -o get-platformio.py \
 && python3 get-platformio.py
ENV PATH=$PATH:/root/.platformio/penv/bin

RUN pio platform install espressif8266 --with-package framework-arduinoespressif8266 \
 && pio platform install espressif32 \
 && cat /root/.platformio/platforms/espressif32/platform.py \
 && chmod 777 /root/.platformio/platforms/espressif32/platform.py \
 && sed -i 's/~2/>=1/g' /root/.platformio/platforms/espressif32/platform.py \
&& cat /root/.platformio/platforms/espressif32/platform.py

# Install NodeJs
RUN apt-get update && apt install -y --no-install-recommends nodejs npm
RUN npm install -g n \
 && n 14.16.0 \
 && rm `which node` \
 && rm `which npm` 
ENV PATH=$PATH:/usr/local/n/versions/node/14.16.0/bin/

# Install gulp globally
RUN npm install -g gulp

# Customer binary files merge tool with specified offset
COPY binary_merger.py /opt/binary_merger.py
